#!./sf/bin/python
# @Author: Michel Andy <ragnar>
# @Date:   2019-04-14T09:25:36-04:00
# @Email:  Andymic12@gmail.com
# @Filename: genetic_learn.py
# @Last modified by:   ragnar
# @Last modified time: 2019-04-14T20:14:30-04:00

import face_alignment
from skimage import io
import plt_util
import numpy as np

fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, device='cpu', flip_input=True)
input = io.imread('face-alignment/test/assets/aflw-test.jpg')
preds = fa.get_landmarks(input)[-1]
plt_util.plot_img(input, preds)

print(np.array(preds).shape)
