# @Author: Michel Andy <ragnar>
# @Date:   2019-04-14T10:05:46-04:00
# @Email:  Andymic12@gmail.com
# @Filename: genetic_network.py
# @Last modified by:   ragnar
# @Last modified time: 2019-04-14T20:18:52-04:00

from __future__ import division, print_function, unicode_literals
import numpy as np
import os
import sys
from datetime import datetime
import tensorflow as tf
from functools import partial
import matplotlib
import matplotlib.pyplot as plt
import skimage.io as io
from PIL import Image
import argparse

'''
Learning patterns from facial alignment data
'''
# MatplotLib default config
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12

# TensorBoard Setup
NOW = datetime.utcnow().strftime('%Y%m%d%H%M%S')
ROOT_LOGDIR = 'network_logs'
LOG_DIR = '{}/'.format(ROOT_LOGDIR)

# Location of generated images
DATA_DIR = './genetic_network_features'
MODEL_DIR = './genetic_network_model'
MODEL_NAME = 'genetic_network.ckpt'
TFRECORDS_TRAIN_FILENAME = 'orlfaces_train_db.tfrecords' #361
TFRECORDS_TEST_FILENAME = 'orlfaces_test_db.tfrecords' #40


# Network layers configuration
TRAIN_TOTAL = 361
TEST_TOTAL = 40
IMAGE_HEIGHT = 50
IMAGE_WIDTH  = 50
CHANNELS = 1

# Layers configuration
INPUT_SIZE = IMAGE_HEIGHT * IMAGE_WIDTH * CHANNELS
HIDDEN_LAYER_1 = 1500
HIDDEN_LAYER_2 = 500  # codings
HIDDEN_LAYER_3 = HIDDEN_LAYER_1
OUTPUT_LAYER = INPUT_SIZE
LEARN_RATE = 0.0001
L2_REG = 0.0001

# Training configuration
BATCH_SIZE = 20

# Total images / BATCH_SIZE,  split over two phases
BATCH_NUM = int(TRAIN_TOTAL / BATCH_SIZE)
QUEUE_CAP = 1000
NUM_AFTER_DEQUEUE = 200
EPOCHS = 1000

# Tensor Size
X = tf.placeholder(tf.float32, shape=[None, INPUT_SIZE])

activation = tf.nn.elu
# https://developers.google.com/machine-learning/crash-course/regularization-for-simplicity/l2-regularization
regularizer = tf.contrib.layers.l2_regularizer(L2_REG)
initializer = tf.contrib.layers.variance_scaling_initializer()


weights1_init = initializer([INPUT_SIZE, HIDDEN_LAYER_1])
weights2_init = initializer([HIDDEN_LAYER_1, HIDDEN_LAYER_2])
weights3_init = initializer([HIDDEN_LAYER_2, HIDDEN_LAYER_3])
weights4_init = initializer([HIDDEN_LAYER_3, OUTPUT_LAYER])

weights1 = tf.Variable(weights1_init, dtype=tf.float32, name='weights1')
weights2 = tf.Variable(weights2_init, dtype=tf.float32, name='weights2')
weights3 = tf.Variable(weights3_init, dtype=tf.float32, name='weights3')
weights4 = tf.Variable(weights4_init, dtype=tf.float32, name='weights4')

biases1 = tf.Variable(tf.zeros(HIDDEN_LAYER_1), name='biases1')
biases2 = tf.Variable(tf.zeros(HIDDEN_LAYER_2), name='biases2')
biases3 = tf.Variable(tf.zeros(HIDDEN_LAYER_3), name='biases3')
biases4 = tf.Variable(tf.zeros(OUTPUT_LAYER), name='biases4')

hidden1 = activation(tf.matmul(X, weights1) + biases1)
hidden2 = activation(tf.matmul(hidden1, weights2) + biases2)
hidden3 = activation(tf.matmul(hidden2, weights3) + biases3)
outputs = tf.matmul(hidden3, weights4) + biases4

# Save model
saver = tf.train.Saver()

# Creating fifo queue from tfrecords
filename_queue_train = tf.train.string_input_producer([TFRECORDS_TRAIN_FILENAME], num_epochs=EPOCHS)
filename_queue_test = tf.train.string_input_producer([TFRECORDS_TEST_FILENAME])

file_writer = tf.summary.FileWriter(LOG_DIR, tf.get_default_graph())

# GPU Config Options
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9

def save_figure(filename, tight_layout=True):
    try:
        path = os.path.join(DATA_DIR, filename + '.png')
        print('Saving figure', path)
        if tight_layout:
            plt.tight_layout()

        if not os.path.isdir(DATA_DIR):
            os.mkdir(DATA_DIR)

        plt.savefig(path, format='png', dpi=300)
        print('Figure saved')
    except Exception as ex:
        print('Error saving file: ', path)
        print(ex)

def get_tf_queue(filename_queue):
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    features = tf.parse_single_example(
      serialized_example,
      # Defaults are not specified since both keys are required.
      features={
        'height': tf.FixedLenFeature([], tf.int64),
        'width': tf.FixedLenFeature([], tf.int64),
        'image_raw': tf.FixedLenFeature([], tf.string),
        })

    image = tf.decode_raw(features['image_raw'], tf.uint8)
    height = tf.cast(features['height'], tf.int32)
    width = tf.cast(features['width'], tf.int32)

    image = tf.reshape(image, [height, width, CHANNELS])
    resized_image = tf.reshape(image, [IMAGE_HEIGHT, IMAGE_WIDTH, CHANNELS])

    print("Resized shape: ", resized_image.shape)

    image_shape = tf.stack([INPUT_SIZE])
    image = tf.reshape(resized_image, image_shape)

    print("flattened tensor shape: ", image.shape)

    image_queue = tf.train.shuffle_batch( [image], batch_size=BATCH_SIZE, capacity=QUEUE_CAP,
    num_threads=4, min_after_dequeue=NUM_AFTER_DEQUEUE, allow_smaller_final_batch=True)

    return image_queue

def plot(image, shape=[IMAGE_HEIGHT, IMAGE_WIDTH, CHANNELS]):

    if CHANNELS == 1:
        plt.imshow(image.reshape(shape[0],shape[1]), cmap='Greys', interpolation='nearest')
    else:
        image = image.astype(float)
        plt.imshow(image.reshape(shape), cmap='Greys', interpolation='nearest')
    plt.axis('off')

def network_features(model_name=MODEL_NAME):
    with tf.Session() as sess:
        saver.restore(sess, os.path.join(MODEL_DIR, model_name))
        weights = [weights1.eval(),weights2.eval(),weights3.eval(), weights4.eval()]
        heights = [IMAGE_HEIGHT, 50, 25]
        widths = [IMAGE_WIDTH, 30, 20]

        for it in range(len(weights)-1):
            weight = weights[it]
            for i in range(5):
                plot(weight.T[i], shape=[heights[it], widths[it], CHANNELS])
                plt.show()

def input_output_compare(model_name=MODEL_NAME, outputs_layer=OUTPUT_LAYER, test_queue=filename_queue_test, n_images=5):
    init = (tf.global_variables_initializer(), tf.local_variables_initializer())
    with tf.Session() as sess:
        sess.run(init)

        saver.restore(sess, os.path.join(MODEL_DIR, MODEL_NAME))
        X_test = get_tf_queue(test_queue)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        try:
            X_test = sess.run([X_test])
            print('X_test shape: ', np.array(X_test).shape)
            outputs_value = outputs.eval(feed_dict={X: X_test[0]})
            fig = plt.figure(figsize=(8, 3 * n_images))

            #first dim is useless
            X_test = X_test[0]

            for img_n in range(n_images):
                #chose random image from batch
                rd = np.random.randint(0, BATCH_SIZE)
                plt.subplot(n_images, 2, img_n * 2 + 1)
                plot(X_test[rd])
                plt.subplot(n_images, 2, img_n * 2 + 2)
                plot(outputs_value[rd])

            save_figure('reconstructed_faces')
            plt.show()

        except Exception as ex:
            print('Unexpected exception thrown: ', ex)
        finally:
            coord.request_stop()
        coord.join(threads)
        sess.close()

def train_network(train_queue, test_queue):
    # He initialization
    he_init = tf.contrib.layers.variance_scaling_initializer()
    l2_regularizer = tf.contrib.layers.l2_regularizer(L2_REG)
    dense_layer = partial(tf.layers.dense,
                             activation=tf.nn.elu,
                             kernel_initializer=he_init,
                             kernel_regularizer=l2_regularizer)

    hidden1 = dense_layer(X, HIDDEN_LAYER_1)
    hidden2 = dense_layer(hidden1, HIDDEN_LAYER_2)
    hidden3 = dense_layer(hidden2, HIDDEN_LAYER_3)
    outputs = dense_layer(hidden3, OUTPUT_LAYER, activation=None)

    reconstruction_loss = tf.reduce_mean(tf.square(outputs - X))
    cost_summary = tf.summary.scalar('PRL1', reconstruction_loss)
    reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
    loss = tf.add_n([reconstruction_loss] + reg_losses)

    optimizer = tf.train.AdamOptimizer(LEARN_RATE)
    training_op = optimizer.minimize(loss)
    saver = tf.train.Saver()
    init = (tf.global_variables_initializer(), tf.local_variables_initializer())

    with tf.Session() as sess:
        sess.run(init)
        train_data = get_tf_queue(train_queue)
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        print('Number of batches: ', BATCH_NUM)
        for epoch in range(EPOCHS):
            for iteration in range(BATCH_NUM):
                print("\r{}%".format(100 * iteration // BATCH_NUM), end="")
                sys.stdout.flush()
                X_batch = sess.run([train_data])
                sess.run(training_op, feed_dict={X: X_batch[0]})
            loss_train = reconstruction_loss.eval(feed_dict={X: X_batch[0]})
            summary = cost_summary.eval(feed_dict={X: X_batch[0]})
            step = epoch * BATCH_NUM  + iteration
            file_writer.add_summary(summary, step)
            print("\r{}".format(epoch+1), "Train MSE:", loss_train)
            saver.save(sess, os.path.join(MODEL_DIR, MODEL_NAME))

        coord.request_stop()
        coord.join(threads)
        sess.close()

parser = argparse.ArgumentParser()
parser.add_argument('-a','--train', action='store_true', help='train the network')
parser.add_argument('-t','--test', action='store_true', help='shows the reconstructed inputs')
parser.add_argument('-f','--features', action='store_true', help='show features learnt by  model')
args = parser.parse_args()

if __name__=='__main__':
    if args.train:
        train_network(filename_queue_train, filename_queue_test)
    if args.features:
        network_features(model_name=MODEL_NAME)
    if args.test:
        input_output_compare()
