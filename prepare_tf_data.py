# @Author: Michel Andy <ragnar>
# @Date:   2019-04-14T10:05:46-04:00
# @Email:  Andymic12@gmail.com
# @Filename: prepare_tf_data.py
# @Last modified by:   ragnar
# @Last modified time: 2019-04-14T18:51:25-04:00


from PIL import Image
import numpy as np
import skimage.io as io
import tensorflow as tf
import glob
import time
import sys, os
import argparse

#default values
DIR = 'raw_faces'
EXT = 'png'
SPLIT = .10

# Get files
FILES = glob.glob('{}/*.{}'.format(DIR,EXT))
FILES = sorted([f for f in FILES])
train_test_split_num = int(len(FILES) * SPLIT)
TRAIN_SET = FILES[train_test_split_num:]
TEST_SET  = FILES[:train_test_split_num]

print('Files found: {}'.format(len(FILES)))
print('train set#: {} and test set#: {}'.format(len(TRAIN_SET), len(TEST_SET)))

def resize_images(files, output_dir, size=[50,50]):
    try:
        for fi in files:
            print(fi)
            path = '{}/{}'.format(output_dir, os.path.basename(fi))
            img = Image.open(fi)
            img = img.resize((50,50), Image.ANTIALIAS)
            print('Resized file: ', path)
            img.save(path)
    except Exception as ex:
        print('Unexpected error while processing: ', path)

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _floats_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value.tolist()))

def generate_face_db(files, tf_filename):
    count = 0 # number of files processed
    err_count = 0
    start_time = 0
    error_filename = 'corrupted_files.txt'
    corrupted_files = open(error_filename,'a+')
    writer = tf.python_io.TFRecordWriter(tf_filename)
    print('Creating: ', tf_filename)
    try:
        for img_path in files:
            count+=1
            print("\r{}%".format(100 * count // len(files)), end="")
            # print('Processing file: {}, count: {}'.format(img_path, count))

            start_time = time.time()
            raw_img = np.array(Image.open(img_path))

            # print('Image shape: {}'.format(raw_img.shape))
            # print('Reading record took: {}'.format(time.time() - start_time))

            #Storing image size
            height = raw_img.shape[0]
            width = raw_img.shape[1]

            print('Image shape: {}'.format(raw_img.shape))
            #flattens image to 1-D
            raw_img = raw_img.tostring()
            example = tf.train.Example(features=tf.train.Features(feature={
                'height': _int64_feature(height),
                'width': _int64_feature(width),
                'image_raw': _bytes_feature(raw_img)}))

            start_time = time.time()
            writer.write(example.SerializeToString())

            print('Writing tfrecord took: {}'.format(time.time() - start_time))
    except Exception as ex:
        print('corrupted file error: ', ex)
        err_count+=1
        corrupted_files.write('{}\n'.format(img_path))
    finally:
        if err_count == 0:
            try:
                os.remove(error_filename)
            except OSError as ose:
                print('could not remove: ', filename)

    writer.close()
    print('\ntotal corrupted files: {}'.format(err_count))


if __name__=='__main__':
    # resize_images(FILES, 'raw_faces')
    generate_face_db(TRAIN_SET, 'orlfaces_train_db.tfrecords')
    generate_face_db(TEST_SET, 'orlfaces_test_db.tfrecords')
